# CC3K -based Roguelike

Written in Java, using AsciiPanel for graphics

The Player lands in a procedurally generated map, which contains rooms, or areas the Player can move in, and walls, that separate the rooms,
and which the Player can not move on.

Each Map will contain a number of randomly spawned NPC character and items. The items are stationary, while the NPCs may move about
the map like the Player. Some NPCs will attack the Player once they are close enough.

There is a Finish for each Map, which the Player can move on, and which will transport the Player to another Map.

[User Manual](https://gitlab.com/mikkot/cc3k/-/blob/master/documentation/player_manual.md)

## Development

Run the project with Gradle
`./gradlew.bat run`

Run unit tests:
`./gradlew.bat test`

Build a JAR:
`./gradlew.bat build`

Run the built project from the JAR: (check path is correct)
`java -jar .\build\libs\cc3k-1.0-SNAPSHOT.jar`

## Dependencies

### Local dependencies
Local dependencies are located in the libs -folder at the root level of this repository.

| Name        | Description                                                                             |
|-------------|-----------------------------------------------------------------------------------------|
| Ascii-Panel | Cloned and built from https://github.com/trystan/AsciiPanel It is used for the game UI. |

### External dependencies (not included in the repo itself)

| Name   | Version                  | Description       |
|--------|--------------------------|-------------------|
| JUnit  | See Gradle configuration |  For unit testing |
| Java   | 14                       |
| Gradle | 8.x.x                    | 