# Player manual

## Movement

| Key | Action |
| ------ | ------ |
| A | Move and change Character direction Left |
| D | Move and change Character direction Right |
| W | Move and change Character direction Up |
| S | Move and change Character direction Down |
| Space | Pick up / Collect |


## Camera

| Key | Action |
| ------ | ------ |
| Arrow Left | Move Camera Left |
| Arrow Right | Move Camera Right |
| Arrow Up | Move Camera Up |
| Arrow Down | Move Camera Down |

The Camera can be moved independently of the Player Character. Currently it will consume a game "turn" or "frame".

## Interaction

| Key | Action |
| ------ | ------ |
| C | Toggle Character Screen |
| 0-9 | Attack NPC around Player |

The current implementation does not allow the Player to choose which surrounding NPC to attack.


## Character Screen

| Key | Action |
| ------ | ------ |
| 0-9 | Select Inventory Item |

Selecting an Inventory Item will use that item.

## Items

Currently, there are 7 items defined in the game that the Player character can pick up by walking over them
and pressing the Pickup/Collect -button:

| Consumables | Weapons | Armor |
| ------ | ------ | ------ |
| Health potion | Sword | Shield |
| | Axe | Armor |
| | Spear | |
| | Dagger | |


| Item | Effect |
| ------ | ------ |
| Health potion | Increases Player health by 50 |
| Shield | Increases Player defence |
| Axe | Increases Player attack |
| Armor | Increases Player defence |
| Spear | Increases Player attack |
| Dagger | Increases Player attack |


## Terrain

There are 3 types of 'ground tiles', or terrain defined in the game, of which currently 2 are in use (floor, wall).

| Tile | Properties |
| ------ | ------ |
| Floor | Traversable by all Characters. Can hold Items. |
| Wall | Non-traversable by any Characters. Can not hold Items. Destroyable. (converts to Floor upon destruction) |
| Water | Traversable by some Characters. NOT IMPLEMENTED YET. |
