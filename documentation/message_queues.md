# Message Queues within the project

The project contains an integrated implementation of a message queue.
The purpose of integrating a message queue to the project is to increase decoupling between game objects.
There are pre-defined queues in 'src.main.java.engine.common.messagequeue.MessageQueues' -enum.

These pre-defined queues are used by all objects communicating through messages.
No dynamic queue creation is possible.

Each GameObject will be subscribed to their queues upon creation.
Even if a GameObject is not subscribed to any queues, they are able to send messages to any queue.
The GameObject must define the queue they are sending each message to.

## Message queue structure

The message queue system consists of several parts:

| Object               | Type      | Explanation                                                                                                             |
|----------------------|-----------|-------------------------------------------------------------------------------------------------------------------------|
| Message              | Class     | Represents an individual message in the system passed from an entity to a MessageQueue and from there to another entity |
| MessageQueue         | Class     | Represents a message queue, which receives messages and stores them until they are passed on to the receiver            |
| MessageQueueManager  | Class     | Stores and manages the message queues. Queues are created here statically.                                              |
| MessageQueues        | Enum      | Lists all the existing message queues                                                                                   |
| IMessageBroker       | Interface |                                                                                                                         |
| DefaultMessageBroker | Class     | Has a MessageQueueManager and is responsible for using the manager to add sent messages to the queues.                  |