package messagequeue;

import engine.common.messagequeue.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DefaultMessageBrokerTest {
    @Test
    public void sendMessage() {
        MessageQueueManager queueManager = new MessageQueueManager();
        DefaultMessageBroker broker = new DefaultMessageBroker(queueManager);

        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .getQueueLength(),
                0
        );

        broker.sendMessage("First", MessageQueues.GAME_OBJECT);

        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .getQueueLength(),
                1
        );

        broker.sendMessage("Second", MessageQueues.GAME_OBJECT);

        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .getQueueLength(),
                2
        );

        broker.sendMessage(
                "Third",
                MessageQueues.GAME_OBJECT,
                "receiver"
        );

        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .getQueueLength(),
                3
        );
        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .peekNextMessage()
                        .getMessage(),
                "First"
        );
        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .consumeNextMessage()
                        .getTargetObject(),
                ""
        );
        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .consumeNextMessage()
                        .getMessage(),
                "Second"
        );
        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .peekNextMessage()
                        .getMessage(),
                "Third"
        );
        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .consumeNextMessage()
                        .getTargetObject(),
                "receiver"
        );
    }

    @Test
    public void sendMultipleMessages() {
        MessageQueueManager queueManager = new MessageQueueManager();
        DefaultMessageBroker broker = new DefaultMessageBroker(queueManager);

        List<String> messages = new ArrayList<>(Arrays.asList("First", "Second", "Third"));

        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .getQueueLength(),
                0
        );
        broker.sendMultipleMessages(messages, MessageQueues.GAME_OBJECT);
        assertEquals(
                queueManager
                        .getMessageQueue(MessageQueues.GAME_OBJECT)
                        .getQueueLength(),
                3
        );
    }
}
