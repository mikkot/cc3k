package messagequeue;

import engine.GameObject;
import engine.common.messagequeue.MessageQueue;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static java.util.Map.entry;
import static org.junit.Assert.*;

public class MessageQueueTest {

    @Test
    public void removeSubscriber() {
        GameObject first = new GameObject();
        GameObject second = new GameObject();
        MessageQueue queue = new MessageQueue(
                new HashMap<String, GameObject>(
                        Map.ofEntries(
                                entry(first.getGameTag(), first),
                                entry(second.getGameTag(), second)
                        )
                )
        );

        assertEquals(queue.getSubscribers().size(), 2);
        queue.removeSubscriber(first.getGameTag());
        assertEquals(queue.getSubscribers().size(), 1);
        assertEquals(queue.isObjectSubscribed(first.getGameTag()), false);
        assertEquals(queue.isObjectSubscribed(second.getGameTag()), true);
    }

    @Test
    public void addSubscriber() {
        MessageQueue queue = new MessageQueue();
        assertEquals(queue.getSubscribers().size(), 0);

        GameObject newObject = new GameObject();
        queue.addSubscriber(newObject);

        assertEquals(queue.getSubscribers().size(), 1);
        assertTrue(queue.isObjectSubscribed(newObject.getGameTag()));
    }
}
