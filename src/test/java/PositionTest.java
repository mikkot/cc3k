import engine.Position;
import org.junit.Test;

import static org.junit.Assert.*;

public class PositionTest {

    @Test
    public void move() {
    }

    @Test
    public void testMove() {
        Position pos = new Position(0, 0);
        pos.move(1,1);
        assertEquals(pos.getX(), 1);
        assertEquals(pos.getY(), 1);

        pos.move(10, 10);
        assertEquals(pos.getX(), 11);
        assertEquals(pos.getY(), 11);

        pos.move(-3, -3);
        assertEquals(pos.getX(), 8);
        assertEquals(pos.getY(), 8);
    }

    @Test
    public void isBetween() {
        Position start = new Position(0, 0);
        Position end = new Position(10, 10);
        Position between = new Position(5, 5);

        assertTrue(between.isBetween(start, end));

        between = new Position(0, 0);
        assertFalse(between.isBetween(start, end));
        between = new Position(10, 10);
        assertFalse(between.isBetween(start, end));
        between = new Position(11, 10);
        assertFalse(between.isBetween(start, end));
        between = new Position(1, 1);
        assertTrue(between.isBetween(start, end));
    }

    @Test
    public void isNextTo() {
        Position p1 = new Position(0, 0);
        Position p2 = new Position(0, 1);
        assertTrue(p1.isNextTo(p2));

        p2 = new Position(0, 2);
        assertFalse(p1.isNextTo(p2));

        p2 = new Position(1, 0);
        assertTrue(p1.isNextTo(p2));

        p2 = new Position(2, 0);
        assertFalse(p1.isNextTo(p2));

        p2 = new Position(0, 0);
        assertTrue(p1.isNextTo(p2));

        p1 = new Position(5, 5);
        p2 = new Position(6, 6);
        assertTrue(p1.isNextTo(p2));


    }
}