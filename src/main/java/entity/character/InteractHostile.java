package entity.character;

import engine.Defence;
import engine.world.World;

public class InteractHostile implements IInteractBehavior {
    public InteractHostile() {}

    public boolean interact(World world, Character agent) {
        Character target = world.getPlayer();
        if(target.getPosition().isNextTo(agent.getPosition())) {
            //if target is next to agent then attack it
            target.takeDamage(agent.getAttack(), Defence.BLUNT);
            return true;
        }
        return false;
    }
}
