package entity.character;

import engine.world.Tile;
import engine.world.World;
import entity.item.Item;
import entity.item.Potion;
import entity.item.Weapon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * This drop behavior drops one random item it selects from a predefined list which contains all basic items in the game.
 */
public class DefaultRandomDropBehavior implements IDropBehavior{
    public DefaultRandomDropBehavior() {}

    @Override
    public List<Item> dropItems(World world, Character character) {
        Class<?>[] possibleClasses = new Class[]{ Potion.class, Weapon.class};

        Random random = new Random();
        int classIndex = random.nextInt(2);

        Class<?> selectedClass = possibleClasses[classIndex];
        String itemSubclassName = selectedClass.getSimpleName();

        Item droppedItem;

        switch (itemSubclassName) {
            case "Potion":
                try {
                    droppedItem = (Potion) selectedClass.getDeclaredConstructor().newInstance();
                } catch (Exception e) {
                    System.out.println("Unable to instantiate item Potion as a random drop!");
                    return new ArrayList<>();
                }
                break;
            case "Weapon":
                try {
                    droppedItem = (Weapon) selectedClass.getDeclaredConstructor().newInstance();
                } catch (Exception e) {
                    System.out.println("Unable to instantiate item Weapon as a random drop!");
                    return new ArrayList<>();
                }
                break;
            default:
                return new ArrayList<>();
        }

        Tile itemDestination = world.getGameMap()[character.getPosition().getX()][character.getPosition().getY()];
        itemDestination.addItem(droppedItem);
        world.addEntityContainedInTile(itemDestination);
        return Arrays.asList(droppedItem);
    }
}
