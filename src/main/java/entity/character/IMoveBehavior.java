package entity.character;

import engine.world.Tile;

public interface IMoveBehavior {
    public void move(Tile[][] map, Character character, int newX, int newY);
}
