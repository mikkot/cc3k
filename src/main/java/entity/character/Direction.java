package entity.character;

public enum Direction {
    UP("up"),
    DOWN("down"),
    LEFT("left"),
    RIGHT("right");

    private final String text;

    Direction(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
