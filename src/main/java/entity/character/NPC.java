package entity.character;

import engine.Position;
import engine.world.Glyph;
import engine.world.Tile;
import engine.world.World;

import java.awt.*;
import java.util.List;

public class NPC extends Character {
    private Position moveLimitTopLeft;
    private Position moveLimitBottomRight;


    //Creates a barebones NPC, cant be used before proper position and limit are set
    public NPC() {
        super(new Glyph(new Color(255, 255, 255), 'H'), 100, 10, 10, new Position(0, 0));
        this.moveLimitTopLeft = new Position(0, 0);
        this.moveLimitBottomRight = new Position(0, 0);
        this.dropBehavior = new DefaultRandomDropBehavior();
    }

    //This one is used with the subclass spawner, only randomMove and subclass specific fields are given
    //Positions need to be set afterwards
    public NPC(Glyph glyph, boolean hostile, int health, int attack, int defence) {
        super(glyph, health, attack, defence, new Position(0, 0));
        this.moveLimitTopLeft = new Position(0, 0);
        this.moveLimitBottomRight = new Position(0, 0);
    }

    //Creates a full Game-Ready NPC with all attributes set
    //mLTL (TopLeft) and mLBR (BottomRight) are the limits inside which the NPC is allowed to move. Like a Room or the whole Map etc.
    public NPC(Glyph glyph, boolean hostile, int health, int attack, int defence,
               Position pos, Position mLTL, Position mLBR) {
        super(glyph, health, attack, defence, pos);
        this.moveLimitTopLeft = mLTL;
        this.moveLimitBottomRight = mLBR;
    }

    @Override
    public void update(World world) {
        //If there is something next to a hostile npc.NPC, it will attack instead of moving around
        if(this.interactBehavior.interact(world, this)) { return; }
        this.moveBehavior.move(world.getGameMap(), this, 0, 0);
    }

    public void setMoveLimitTopLeft(Position moveLimitTopLeft) {
        this.moveLimitTopLeft.setPosition(moveLimitTopLeft);
    }
    public void setMoveLimitBottomRight(Position moveLimitBottomRight) {
        this.moveLimitBottomRight.setPosition(moveLimitBottomRight);
    }

    public void setMoveLimits(Position topLeft, Position bottomRight) {
        setMoveLimitTopLeft(topLeft);
        setMoveLimitBottomRight(bottomRight);
    }

    public boolean charactersInReach(World world) {
        List<Tile> surroundingTiles = world.getNeighbouringTiles(getPosition());
        for(Tile t : surroundingTiles) {
            if(t.hasCharacter()) {
                return true;
            }
        }
        return false;
    }

    public Character getPlayer(World world) {
        List<Tile> surroundingTiles = world.getNeighbouringTiles(getPosition());
        for(Tile t : surroundingTiles) {
            if(t.hasPlayer()) {
                return t.getCharacter();
            }
        }
        return null;
    }

    public boolean playerInReach(World world) {
        List<Tile> surroundingTiles = world.getNeighbouringTiles(getPosition());
        for(Tile t : surroundingTiles) {
            if(t.hasPlayer()) {
                return true;
            }
        }
        return false;
    }

    public NPC clone() {
        return new NPC();
    }
}
