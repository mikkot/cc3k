package entity.character;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import engine.*;
import engine.world.Glyph;
import engine.world.MapPrintable;
import engine.world.Tile;
import engine.world.World;
import entity.item.Item;

public class Character extends GameObject implements ICharacter, MapPrintable {
    private Glyph mapTag;
    private int health;
    private int maxHealth;
    private int attack;
    private int defence;
    private Position position;
    private String type;
    private String race = "unknown";
    private boolean isDead;
    private boolean toBeRemoved = false;
    private int xp;
    private int xpLimit = 100;
    private int level;
    private int xpYield = 50;
    private String direction = Direction.LEFT.toString();

    // Changeable Character capabilities
    protected IInteractBehavior interactBehavior;
    protected IMoveBehavior moveBehavior;
    protected IInventory inventory;
    protected IDropBehavior dropBehavior;

    // Make this class 'Observable'
    private PropertyChangeSupport onChangeSupport;

    public Character(Glyph glyph, int health, int attack, int defence, Position pos) {
        this.mapTag = glyph;
        this.health = health;
        this.maxHealth = health;
        this.attack = attack;
        this.defence = defence;
        this.position = pos;
        this.type = "entity/character";
        this.isDead = false;
        this.xp = 0;
        this.level = 0;
        this.onChangeSupport = new PropertyChangeSupport(this);
    }

    public Glyph getGlyph() { return this.mapTag; }
    public void setGlyph(Glyph glyph) { this.mapTag = glyph; }

    public String getRace() { return this.race; }
    public void setRace(String race) { this.race = race; }

    public String getType() { return this.type; }

    //Death
    public boolean isDead() { return this.isDead; }
    public void setIsDead(boolean dead) { this.isDead = dead; }
    public void die() {
        setIsDead(true);
        setIsToBeRemoved(true);
        // Send message to MessageQueue that this Character has died. The message should contain a reference to the
        // Character object sending it.
    }
    public List<Item> dropOnDeath(World world) {
        if(this.getDropBehavior() != null) {
            return this.getDropBehavior().dropItems(world, this);
        }
        return new ArrayList<>();
    }
    public void setIsToBeRemoved(boolean value) { this.toBeRemoved = value; }
    public boolean isToBeRemoved() { return this.toBeRemoved; }

    //Movement
    public void setMoveBehavior(IMoveBehavior moveBehavior) { this.moveBehavior = moveBehavior; }
    public IMoveBehavior getMoveBehavior() { return this.moveBehavior; }
    public void move(Tile[][] map, int x, int y) {
        this.changeDirection(x, y);
        this.moveBehavior.move(map, this, x, y);
    }
    public void move(Tile[][] map, Position pos) {
        this.changeDirection(pos.getX(), pos.getY());
        this.moveBehavior.move(map, this, pos.getX(), pos.getY());
    }

    public void changeDirection(int dirX, int dirY) {
        // Assuming only X xor Y can be changed (not both at the same time)
        if(dirY > 0) {
            this.setDirection(Direction.UP.toString());
        } else if(dirY < 0) {
            this.setDirection(Direction.DOWN.toString());
        } else if(dirX > 0) {
            this.setDirection(Direction.RIGHT.toString());
        } else {
            this.setDirection(Direction.LEFT.toString());
        }

    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return this.direction;
    }

    public void update(World world) {}

    public List<Tile> getNeighbours(World world) {
        return world.getNeighbouringTiles(this.position);
    }

    public void takeDamage(int damage, Defence type) {
        int residual = 0;

        if(type == Defence.POTION) {
            residual = damage;
        } else {
            residual = damage - this.defence;
        }

        if(residual >= 0) {
            onChangeSupport.firePropertyChange("health", this.getHealth(), this.getHealth() - residual);
            this.health -= residual;
            if(this.health <= 0) {
                die();
            }
        }
    }

    public boolean receiveInteraction(Character agent, Item itemUsedToInteract) {
        return false;
    }

    //Character stats
    public int getHealth() { return this.health; }
    public void setHealth(int health) { this.health = health; }
    public int getMaxHealth() { return this.maxHealth; }
    public void setMaxHealth(int maxHealth) { this.maxHealth = maxHealth; }
    public int getAttack() { return this.attack; }
    public int getDefence() { return this.defence; }
    public int getXp() { return this.xp; }
    public void setXp(int xp) { this.xp = xp; }
    public int getXpLimit() { return this.xpLimit; }
    public void setXpLimit(int xpLimit) { this.xpLimit = xpLimit; }
    public int getLevel() { return this.level; }
    public void setLevel(int level) { this.level = level; }
    public int getXpYield() { return this.xpYield; }
    public void setXpYield(int xpYield) { this.xpYield = xpYield; }
    @Override
    public Position getPosition() { return this.position; }
    public void setPosition(Position newPosition) {
        this.position.setPosition(newPosition);
    }
    public void setPosition(int x, int y) {
        this.position.setPosition(x, y);
    }
    public void accumulateXp(int amount) {
        if(amount <= 0) { return; }
        int currentXp = getXp();
        int currentXpLimit = getXpLimit();
        if(currentXp + amount >= currentXpLimit) {
            setLevel(getLevel() + 1);
            setXp(0);
            setXpLimit(currentXpLimit + 100);
            accumulateXp(amount - (currentXpLimit - currentXp));
        } else {
            setXp(currentXp + amount);
        }
    }

    //Inventory
    public void collect(Tile[][] map) {}
    public void useItem(Item item) {
        item.performUse(this);
    }
    public void setInventory(IInventory inventory) { this.inventory = inventory; }
    public IInventory getInventory() { return this.inventory; }
    public void setInteractBehavior(IInteractBehavior interactBhv) {
        this.interactBehavior = interactBhv;
    }

    public IDropBehavior getDropBehavior() { return this.dropBehavior; }
    public void setDropBehavior(IDropBehavior dropBehavior) { this.dropBehavior = dropBehavior; }

    //Interaction
    public void attack(Character target) {
        this.setChanged();
        this.notifyObservers("Player is attacking " + target.getType());
        target.takeDamage(this.attack, Defence.SLASH);
        if(target.isDead()) {
            accumulateXp(target.getXpYield());
        }
    }
    public void interact(World world) {
        if(this.interactBehavior != null) this.interactBehavior.interact(world, this);
    }

    // onChange support
    public void addPropertyChangeListener(PropertyChangeListener changeListener) {
        this.onChangeSupport.addPropertyChangeListener(changeListener);
    }

    @Override
    public String toString() {
     return  this.mapTag + " Health: " + this.health + "\n\tAttack: " + this.attack + "\n\tDefence: " + this.defence;
    }
}
