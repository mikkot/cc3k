package entity.character;

import engine.world.World;

public class InteractFriendly implements IInteractBehavior {
    public InteractFriendly() {}

    public boolean interact(World world, Character agent) {
        System.out.println("Interacting friendly!");
        return false;
    }
}
