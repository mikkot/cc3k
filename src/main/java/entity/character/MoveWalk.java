package entity.character;

import engine.Position;
import engine.world.Tile;

public class MoveWalk implements IMoveBehavior {
    public void move(Tile[][] map, Character character, int x, int y) {
        System.out.println("Walking...");
        Position currentPosition = character.getPosition();
        // Do not try to move outside the map
        int newXPos = currentPosition.getX() + x;
        int newYPos = currentPosition.getY() + y;
        if(newXPos < 0 || newXPos >= map.length || newYPos < 0 || newYPos >= map[0].length) return;

        Tile t = map[currentPosition.getX() + x][currentPosition.getY() + y];
        //Making sure Characters do not try to move on an engine.world.Tile that already has a entity.character.Character on it
        if(t.freeToWalk()) {
            map[currentPosition.getX()][currentPosition.getY()].setCharacter(null);
            currentPosition.move(x, y);
            t.moveInto(character);
            //The Map / Game / World should take care of mapping the GameObjects to the Map!
        }
    }
}
