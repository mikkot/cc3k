package entity.character;

import entity.item.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class InventoryDefault implements IInventory {
    boolean isOpen;
    List<Item> items;

    public InventoryDefault() {
        this.isOpen = false;
        this.items = new ArrayList<>();
    }


    @Override
    public void add(Item item) {
        String gameTag = item.getGameTag();
        items.add(item);
    }

    @Override
    public void remove(Item item) {
        String gameTag = item.getGameTag();
        //if list contains it, remove it
        this.items.removeIf(i -> i.getGameTag().equals(gameTag));
    }

    @Override
    public Item getItem(String type) {
        for(Item i : this.items) {
            if(i.getType().equals(type)) {
                return i;
            }
        }
        return null;
    }
    @Override
    public List<Item> getItems() { return this.items; }
    @Override
    public Map<String, Integer> getAggregatedAndSortedItems() {
        Map<String, Integer> itemsAggregated = new HashMap<>();
        for(Item i : this.items) {
            String type = i.getType();
            if(itemsAggregated.containsKey(type)) {
                itemsAggregated.put(type, itemsAggregated.get(type) + 1);
            } else {
                itemsAggregated.put(type, 1);
            }
        }
        return itemsAggregated;
    }

    @Override
    public void open() { this.isOpen = true; }
    @Override
    public boolean isOpen() { return this.isOpen; }
    @Override
    public void toggleOpen() { this.isOpen = !this.isOpen; }

    @Override
    public void close() {}
}
