package entity.character;

import engine.world.World;
import entity.item.Item;

import java.util.List;

public interface IDropBehavior {
    List<Item> dropItems(World world, Character character);
}
