package entity.character;

import engine.Position;
import engine.RandomMove;
import engine.world.Tile;

import java.util.Random;

public class MoveRandom implements IMoveBehavior {
    private RandomMove randomMove;

    public MoveRandom() {
        this.randomMove = new RandomMove(new Random());
    }

    public void move(Tile[][] map, Character character, int x, int y) {
        Position currentPosition = character.getPosition();
        Position p = this.randomMove.getRandomMoveWithinLimits(currentPosition, new Position(0, 0), new Position(map.length-1, map[0].length-1));
        Tile toMoveTo = map[currentPosition.getX() + p.getX()][currentPosition.getY() + p.getY()];
        if(toMoveTo.freeToWalk()) {
            map[currentPosition.getX()][currentPosition.getY()].setCharacter(null);
            currentPosition.move(p);
            toMoveTo.moveInto(character);
        }
    }
}
