package entity.character;

public class AggregatedInventoryItem {
    private String type;
    private int amount;

    public AggregatedInventoryItem(String type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    public String getType() { return this.type; }
    public int getAmount() { return this.amount; }
}
