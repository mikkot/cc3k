package entity.character;

public enum NPCs {
    WEREWOLF("werewolf"),
    VAMPIRE("vampire"),
    TROLL("troll"),
    GOBLIN("goblin"),
    ORC("orc"),
    RAT("rat"),
    UNKNOWN("unknown");

    private final String text;

    NPCs(final String text) { this.text = text; }

    @Override
    public String toString() {
        return this.text;
    }
}
