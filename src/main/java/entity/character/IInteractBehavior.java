package entity.character;

import engine.world.World;

public interface IInteractBehavior {
    boolean interact(World world, Character agent);
}
