package entity.character;

import entity.item.Item;

import java.util.List;
import java.util.Map;

public interface IInventory {
    public void open();
    public void close();
    public void toggleOpen();
    public void add(Item item);
    public void remove(Item item);
    public boolean isOpen();
    public Map<String, Integer> getAggregatedAndSortedItems();
    public List<Item> getItems();
    public Item getItem(String type);
}
