package entity.character;

import engine.Position;
import engine.world.Glyph;
import engine.world.Tile;
import engine.world.World;
import entity.item.Item;

public class Player extends Character {

    public Player(Glyph glyph, int health, int attack, int defence, Position pos) {
        super(glyph, health, attack, defence, pos);
        this.tag = "player";
        this.moveBehavior = new MoveWalk();
        this.interactBehavior = new InteractPlayerDefault();
        this.inventory = new InventoryDefault();
    }

    @Override
    public void update(World world) {

    }
    @Override
    public void collect(Tile[][] map) {
        //Should this functionality be in the Item class?
        System.out.println("Collecting...");
        //Collect item that Player is standing on
        Tile tile = map[this.getPosition().getX()][this.getPosition().getY()];
        Item item = tile.getItem();
        if(item == null) {
            System.out.println("Nothing to collect!");
            return;
        }
        System.out.println("Collected: " + item.toString());
        this.inventory.add(item);
        tile.clearItems();
        item.collect();
    }
}
