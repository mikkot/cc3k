package entity.character;

import engine.Position;
import engine.world.Tile;
import engine.world.World;

public class InteractPlayerDefault implements IInteractBehavior {
    public boolean interact(World world, Character agent) {
        System.out.println("Interacting in direction " + agent.getDirection());

        Position targetPosition = getPosition(agent);

        // We don't want to act if the target lies outside the World Map
        if(world.isOutsideMap(targetPosition.getX(), targetPosition.getY())) return false;

        Tile targetTile = world.getGameMap()[targetPosition.getX()][targetPosition.getY()];

        // If target Tile contains a Character, interact with that, otherwise interact with the Tile itself.
        if(targetTile.hasCharacter()) {
            return targetTile.getCharacter().receiveInteraction(agent, null);
        } else {
            return targetTile.receiveInteraction(agent, null);
        }
    }

    private static Position getPosition(Character agent) {
        String agentDirection = agent.getDirection();
        // We can assume the Character is the Player, as this is an Interact-class meant to be used with the Player.
        Position agentPosition = agent.getPosition();
        // We get the position of the target of the interaction by "moving" the agents Position by the agents Direction.
        Position targetPosition = new Position(agentPosition.getX(), agentPosition.getY());

        if(agentDirection.equals(Direction.DOWN.toString())) {
            targetPosition.move(0, -1);
        } else if(agentDirection.equals(Direction.UP.toString())) {
            targetPosition.move(0, 1);
        } else if(agentDirection.equals(Direction.RIGHT.toString())) {
            targetPosition.move(1, 0);
        } else {
            targetPosition.move(-1, 0);
        }
        return targetPosition;
    }
}
