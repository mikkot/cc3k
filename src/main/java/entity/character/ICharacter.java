package entity.character;

import engine.Defence;
import engine.Position;
import engine.world.Tile;
import engine.world.World;
import entity.item.Item;

import java.beans.PropertyChangeListener;
import java.util.List;

public interface ICharacter {

    public void move(Tile[][] map, int x, int y);
    public void move(Tile[][] map, Position pos);
    public void attack(Character target);
    List<Item> dropOnDeath(World world);
    public void die();
    public void takeDamage(int damage, Defence type);
    public void update(World world);
    public int getHealth();
    public int getAttack();
    public int getDefence();
    public void collect(Tile[][] map);
    public void addPropertyChangeListener(PropertyChangeListener changeListener);
}
