package entity.item;

import engine.Position;
import engine.world.Glyph;
import entity.item.behavior.IEquipBehavior;
import entity.item.behavior.IUseBehavior;

import java.awt.*;

public class Armor extends Item {

    public Armor() {
        super(new Glyph(new Color(255, 255, 255), 'A'), new Position(0, 0));
    }

    public Armor(Color color) {
        super(new Glyph(color, 'A'), new Position(0, 0));
    }

    public Armor(IEquipBehavior equipBehavior, IUseBehavior useBehavior) {
        super(new Glyph(new Color(255, 255, 255), 'A'), new Position(0, 0));
        this.equipBehavior = equipBehavior;
        this.useBehavior = useBehavior;
    }

    @Override
    public void collect() {}
    @Override
    public void drop() {}

}
