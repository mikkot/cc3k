package entity.item;

import engine.Position;
import engine.world.Glyph;
import entity.item.behavior.IEquipBehavior;
import entity.item.behavior.IUseBehavior;

import java.awt.*;

public class Weapon extends Item {
    IEquipBehavior equipBehavior;
    IUseBehavior useBehavior;

    public Weapon() {
        super(new Glyph(new Color(255, 255, 255), 'W'), new Position(0, 0));
        this.setType("Weapon");
    }

    public Weapon(IEquipBehavior equipBehavior, IUseBehavior useBehavior) {
        super(new Glyph(new Color(255, 255, 255), 'W'), new Position(0, 0));
        this.setType("Weapon");
        this.equipBehavior = equipBehavior;
        this.useBehavior = useBehavior;
    }

    public void collect() {}
    public void drop() {}
}
