package entity.item.behavior;

import entity.character.Character;
import entity.item.Item;
import entity.item.Potion;

public class UseDrinkHealth implements IUseBehavior {
    public void use(Character target, Item item) {
        System.out.println("Drinking...");
        Potion potion = (Potion) item;

        int newHealth = target.getHealth() + potion.getValue();
        target.setHealth(Math.min(newHealth, target.getMaxHealth()));
        target.getInventory().remove(item);
    }
}
