package entity.item.behavior;

public interface IEquipBehavior {
    void equip();
    void unequip();
}
