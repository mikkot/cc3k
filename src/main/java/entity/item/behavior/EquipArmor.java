package entity.item.behavior;

public class EquipArmor implements IEquipBehavior {
    public void equip() {
        System.out.println("Equipped...");
    }

    public void unequip() {
        System.out.println("Unequipped...");
    }
}
