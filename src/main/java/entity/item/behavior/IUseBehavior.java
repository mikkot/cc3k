package entity.item.behavior;

import entity.character.Character;
import entity.item.Item;

public interface IUseBehavior {
    public void use(Character target, Item item);
}
