package entity.item;

import engine.*;
import engine.world.Glyph;
import engine.world.MapPrintable;
import engine.world.Tile;
import engine.world.World;
import entity.character.Character;
import entity.item.behavior.IEquipBehavior;
import entity.item.behavior.IUseBehavior;

import java.awt.*;
import java.util.List;

public abstract class Item extends GameObject implements MapPrintable, IItem{
    private Glyph glyph;
    private Position position;
    private String type;
    private boolean toBeRemoved = false;
    protected IUseBehavior useBehavior;
    protected IEquipBehavior equipBehavior;

    public Item(Glyph glyph, Position pos) {
        this.glyph = glyph;
        this.position = pos;
        this.type = "item";
    }

    public String getType() {
        return this.type;
    }
    public void setType(String type) { this.type = type; }

    public Glyph getGlyph() {
        return this.glyph;
    }
    public void setColor(Color color) { this.glyph.setColor(color); }

    public void update(World world) {

    }

    public void use() {

    }

    @Override
    public boolean receiveInteraction(Character agent, Item itemUsedToInteract) {
        return false;
    }

    public void collect() {}
    public void drop() {
        //remove from Player inventory (maybe insert into current World tile Player stands in)
        // Maybe the Item class shouldn't worry about this, but the Inventory keeps track of Items in it, and should
        // also handle dropping items. This could be used as an "after dropping" action, for example, maybe some things
        // react differently when they are dropped.
    }

    /**public Item clone() {
        return new Item('E', new Position(0, 0));
    }*/

    public List<Tile> getNeighbours(World world) {
        return world.getNeighbouringTiles(this.position);
    }

    @Override
    public Position getPosition() {
        return this.position;
    }
    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isToBeRemoved() {
        return this.toBeRemoved;
    }
    public void setUseBehavior(IUseBehavior useBhv) { this.useBehavior = useBhv; }
    public void performUse(Character user) {
        this.useBehavior.use(user, this);
    }
    public void performEquip() {
        this.equipBehavior.equip();
    }
    public void performUnequip() {
        this.equipBehavior.unequip();
    }
    public void setEquipBehavior(IEquipBehavior equipBhv) {
        this.equipBehavior = equipBhv;
    }
}
