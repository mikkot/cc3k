package entity.item;

public interface Consumable {

    void consume();
}
