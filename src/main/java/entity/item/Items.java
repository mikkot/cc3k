package entity.item;

public enum Items {
    HEALTH_POTION("health_potion"),
    SWORD("sword"),
    AXE("axe"),
    SPEAR("spear"),
    DAGGER("dagger"),
    SHIELD("shield"),
    ARMOR("armor");

    private final String text;

    Items(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
