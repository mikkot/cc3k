package entity.item;

import entity.character.Character;

public interface IItem {

    void collect();
    void drop();
    void performUse(Character user);
    void performEquip();
}
