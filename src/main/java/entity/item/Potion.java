package entity.item;

import engine.Position;
import engine.world.Glyph;
import entity.item.behavior.IUseBehavior;
import entity.item.behavior.UseDrinkHealth;

import java.awt.*;

public class Potion extends Item {
    private int value = 50;

    public Potion() {
        super(new Glyph(new Color(255, 255, 255),'P'), new Position(0, 0));
        this.setType("Potion");
        this.useBehavior = new UseDrinkHealth();
    }
    public Potion(Position position) {
        this();
        this.setPosition(position);
    }
    public Potion(IUseBehavior useBehavior) {
        this();
        this.useBehavior = useBehavior;
    }
    public Potion(IUseBehavior useBehavior, Position position) {
        this();
        this.useBehavior = useBehavior;
        this.setPosition(position);
    }
    public Potion(IUseBehavior useBehavior, int x, int y) {
        this();
        this.setPosition(new Position(x, y));
        this.useBehavior = useBehavior;
    }

    public int getValue() { return value; }
    public void setValue(int value) { this.value = value; }
}
