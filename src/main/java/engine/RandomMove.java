package engine;

import java.util.Random;

public class RandomMove {
    private Random rand;

    public RandomMove(Random rand) {
        this.rand = rand;
    }

    public Position getRandomMove() {
        int x = this.rand.nextInt(3) - 1;
        int y = this.rand.nextInt(3) - 1;
        return new Position(x, y);
    }

    public int getPositiveOrZeroValue() {
        return this.rand.nextInt(2);
    }

    public int getNegativeOrZeroValue() {
        return this.rand.nextInt(2) - 1;
    }

    public int getNormalBoundedValue() {
        return this.rand.nextInt(3) - 1;
    }

    /**
     *
     * @param oldPos The current Position of the Character
     * @param topLeftLim The top-left limit of the moving area (ex. a room / map corner)
     * @param bottomRightLim The bottom-left limit of the moving area, like topLeftLim
     * @return  A Position object with the increments for x and y coordinates
     */
    public Position getRandomMoveWithinLimits(Position oldPos, Position topLeftLim, Position bottomRightLim) {
        int x = getNormalBoundedValue();
        int y = getNormalBoundedValue();

        if(oldPos.getX() <= topLeftLim.getX() + 1) {
            //Can only use positive X values
            x = getPositiveOrZeroValue();
        } else if(oldPos.getX() >= bottomRightLim.getX() - 1) {
            //Can only use negative X values
            x = getNegativeOrZeroValue();
        }

        if(oldPos.getY() <= topLeftLim.getY() + 1) {
            //Can only use positive Y values
            y = getPositiveOrZeroValue();
        } else if(oldPos.getY() >= bottomRightLim.getY() - 1) {
            //Can only use negative Y values
            y = getNegativeOrZeroValue();
        }
        return new Position(x, y);
    }
}
