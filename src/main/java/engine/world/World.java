package engine.world;

import engine.NPCspawner;
import engine.Position;
import entity.character.InteractHostile;
import entity.character.Player;
import entity.character.NPC;
import entity.item.Item;
import entity.item.Potion;
import entity.character.Character;
import entity.item.behavior.UseDrinkHealth;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.List;

public class World {
    private Tile[][] gameMap;
    private List<MapPrintable> entities;
    private int mapWidth;
    private int mapHeight;
    //Todo: these variables below this comment should be refactored out of this class, along with creation methods
    private Random random;
    private NPCspawner npcSpawner;
    private Player player;
    private MapGenerator mapGenerator;
    private PropertyChangeListener loggerObserver;

    public World(int width, int height, Random random, NPCspawner npcSpawner, MapGenerator mapGenerator, PropertyChangeListener loggerObserver) {
        this.mapWidth = width;
        this.mapHeight = height;
        this.gameMap = new Tile[width][height];
        this.entities = new ArrayList<>();
        this.random = random;
        this.npcSpawner = npcSpawner;
        this.mapGenerator = mapGenerator;
        this.player = new Player(new Glyph(new Color(100, 255, 100), '@'), 100, 40, 5, new Position(1, 1));
        this.loggerObserver = loggerObserver;

    }

    public Tile[][] getGameMap() {
        return this.gameMap;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void generateMap() {
        //TODO: remove this player addition from here, and move somewhere else
        //TODO: make this randomized, use some algorithm to generate certain kind of Maps
        this.entities.add(this.player);
        this.gameMap = this.mapGenerator.generateTiles().smooth(8).build();

        gameMap[this.getPlayer().getPosition().getX()][this.getPlayer().getPosition().getY()].setCharacter(this.getPlayer());
        setLevelFinishTile();
    }

    //TODO: make this randomized?
    public void setLevelFinishTile() {
        Tile finishTile = this.getGameMap()[this.getGameMap().length - 2][this.getGameMap()[0].length-2];
        finishTile.setEnterBehavior(new EnterFinishLevel(this));
        finishTile.setGlyph(new Glyph(new Color(255, 255, 0), '$'));
        finishTile.setType(Tiles.FLOOR.toString());
        this.changeGlyphsAroundTile(finishTile.getPosition(), 'o', Tiles.FLOOR.toString());
    }

    public void changeGlyphsAroundTile(Position tileLocation, char newGlyph, String tileType) {
        if(isOutsideMap(tileLocation.getX(), tileLocation.getY())) return;
        List<Tile> surroundingTiles = this.getNeighbouringTiles(tileLocation);
        for(Tile tile : surroundingTiles) {
            tile.setGlyph(newGlyph);
            tile.setType(tileType);
        }
    }

    public void resetLevel() {
        this.clearEntities();
        //Re-add Player Inventory content to the Entity list
        this.entities.addAll(this.getPlayer().getInventory().getItems());
        this.generateMap();
        this.createRandomLevelItems(20);
        this.createRandomLevelNPCs(25);
    }
    public void clearEntities() {
        this.entities.clear();
    }

    public boolean isOutsideMap(int x, int y) {
        if(x < 0 || x >= gameMap.length || y < 0 || y >= gameMap[0].length) {
            return true;
        }
        return false;
    }

    public void updateGameEntities() {
        for(MapPrintable mp : this.entities) {
            mp.update(this);
        }
        // After updating, we will remove those marked for removal
        int size = this.entities.size() - 1;
        // And for all the removed entities, we call their dropBehaviour, and add the items they possibly drop
        List<Item> allDroppedItems = new ArrayList<>();
        for(int i = size; i>0; i--) {
            if(this.entities.get(i).isToBeRemoved()) {
                if(this.entities.get(i).getType().equals("entity/character")) {
                    Character character = (Character) this.entities.get(i);
                    allDroppedItems.addAll(character.dropOnDeath(this));
                }
                this.getGameMap()[this.entities.get(i).getPosition().getX()][this.entities.get(i).getPosition().getY()].update(this);
                this.entities.remove(i);
                i--;
            }
        }
        this.entities.addAll(allDroppedItems);
    }
    //TODO: this should probably be refactored and moved to its own component?
    public void createRandomLevelItems(int amount) {
        for(int i = 0; i<amount; i++) {
            int xPosition = random.nextInt(getMapWidth()-2) + 1;
            int yPosition = random.nextInt(getMapHeight()-2) + 1;
            this.getGameMap()[xPosition][yPosition].addItem(new Potion(new UseDrinkHealth(), xPosition, yPosition));
            this.entities.add(this.getGameMap()[xPosition][yPosition].getItem());
        }
    }
    //TODO: this should probably be refactored and moved to its own component?
    public void createRandomLevelNPCs(int amount) {
        List<NPC> npcs = this.npcSpawner.createRandomTypeNPCsOfAmount(10);

        for(int i = 0; i<npcs.size(); i++) {
            NPC npc = npcs.get(i);
            npc.setMoveLimitTopLeft(getOrigin());
            npc.setMoveLimitBottomRight(getEnd());
            int topLimit = random.nextInt(getMapWidth()-2) + 1;
            int bottomLimit = random.nextInt(getMapHeight()-2) + 1;
            npc.setPosition(new Position(topLimit, bottomLimit));
            npc.setInteractBehavior(new InteractHostile());
            npc.addPropertyChangeListener(this.loggerObserver);
            this.entities.add(npc);
        }
    }

    public List<Tile> getNeighbouringTiles(int x, int y) {
        ArrayList<Tile> tiles = new ArrayList<>();
        if(!isOutsideMap(x-1, y)) tiles.add(gameMap[x-1][y]);
        if(!isOutsideMap(x+1, y)) tiles.add(gameMap[x+1][y]);
        if(!isOutsideMap(x-1, y-1)) tiles.add(gameMap[x-1][y-1]);
        if(!isOutsideMap(x, y-1)) tiles.add(gameMap[x][y-1]);
        if(!isOutsideMap(x+1, y-1)) tiles.add(gameMap[x+1][y-1]);
        if(!isOutsideMap(x-1, y+1)) tiles.add(gameMap[x-1][y+1]);
        if(!isOutsideMap(x, y+1)) tiles.add(gameMap[x][y+1]);
        if(!isOutsideMap(x+1, y+1)) tiles.add(gameMap[x+1][y+1]);
        return tiles;
    }

    public List<Tile> getNeighbouringTiles(Position position) {
        return getNeighbouringTiles(position.getX(), position.getY());
    }

    public Position getOrigin() {
        return new Position(0, 0);
    }

    public Position getEnd() {
        return new Position(gameMap.length, gameMap[0].length);
    }

    public Character getPlayer() {
        return this.player;
    }

    public void addEntityContainedInTile(Tile tile) {
        if(!tile.hasItems()) return;
        this.entities.add(tile.getItem());
    }
}
