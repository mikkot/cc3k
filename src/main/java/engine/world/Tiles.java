package engine.world;

public enum Tiles {
    WALL("wall"),
    FLOOR("floor"),
    WATER("water");

    private final String text;

    Tiles(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
