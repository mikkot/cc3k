package engine.world;

import entity.character.Character;

import java.util.Observer;

public class EnterFinishLevel extends EnterTeleport {
    private World world;

    public EnterFinishLevel(World world) {
        super();
        this.world = world;
    };

    protected void resetWorld() {
        this.world.resetLevel();
    }

    @Override
    public void onEnter(Tile tile, Character character) {
        System.out.println("Resetting World!");
        super.onEnter(tile, character);
        if(character.getGameTag().equals(this.world.getPlayer().getGameTag())) {
            resetWorld();
        }
    }
}
