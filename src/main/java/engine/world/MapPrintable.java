package engine.world;

import engine.Position;
import engine.world.Tile;
import engine.world.World;
import entity.character.Character;
import entity.item.Item;

import java.util.List;

public interface MapPrintable {
    Glyph getGlyph();
    Position getPosition();
    void setPosition(Position position);
    void update(World world);
    List<Tile> getNeighbours(World world);
    String getType();
    boolean isToBeRemoved();
    boolean receiveInteraction(Character agent, Item itemUsedToInteract);
}
