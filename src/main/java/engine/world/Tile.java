package engine.world;

import engine.Position;
import engine.world.Tiles;
import entity.character.Character;
import entity.item.Item;

import java.awt.*;
import java.util.List;

public class Tile implements MapPrintable {
    public int xPos;
    public int yPos;
    public String type;
    private Glyph glyph;
    private Item item;
    private Character character;
    private boolean toBeRemoved = false;
    private IEnterBehavior enterBehavior;
    private boolean isDiggable;

    public Tile(int x, int y, String type, Glyph glyph, boolean diggable) {
        this.xPos = x;
        this.yPos = y;
        this.type = type;
        this.glyph = glyph;
        this.isDiggable = diggable;
    }

    public Tile(int x, int y, String type, char glyph, boolean diggable) {
        this(x, y, type, new Glyph(new Color(255, 255, 255), glyph), diggable);
    }

    public Glyph getGlyph() {
        if(this.hasCharacter()) {
            return this.getCharacter().getGlyph();
        } else if(this.hasItems()) {
            return this.getItem().getGlyph();
        }
        return this.glyph;
    }

    public void setGlyph(Glyph newGlyph) {
        this.glyph = newGlyph;
    }
    public void setGlyph(char glyph) { this.glyph.setGlyph(glyph); }

    public boolean isDiggable() {
        return this.isDiggable;
    }

    //entity.character.Character
    public Character getCharacter() {
        return this.character;
    }
    public boolean hasPlayer() {
        if(this.character != null && this.character.getGameTag().equals("player")) {
            return true;
        }
        return false;
    }
    public boolean hasCharacter() {
        if(this.character == null) {
            return false;
        }
        return true;
    }
    public void setCharacter(Character character) {
        this.character = character;
    }
    public void setEnterBehavior(IEnterBehavior enterBhv) { this.enterBehavior = enterBhv; }
    public IEnterBehavior getEnterBehavior() { return this.enterBehavior; }

    public void moveInto(Character character) {
        if(this.getEnterBehavior() != null && this.getEnterBehavior().isActive()) {
            this.performEnterBehavior(character);
            return;
        }
        this.setCharacter(character);
    }

    public void performEnterBehavior(Character character) {
        this.getEnterBehavior().onEnter(this, character);
    }

    public String getType() {
        return this.type;
    }

    public void setType(String newType) {
        this.type = newType;
    }

    public boolean isDead() {
        return false;
    }

    // Items
    public void addItem(MapPrintable item) {
        if(this.item != null) { return; }
        this.item = (Item) item;
    }
    public boolean hasItems() {
        return this.item != null;
    }
    public Item getItem() {
        return this.item;
    }
    public void clearItems() {
        this.item = null;
    }

    public void update(World world) {
        if(this.hasCharacter() && this.getCharacter().isDead()) {
            this.setCharacter(null);
        }
        if(this.hasItems() && this.getItem().isToBeRemoved()) {
            this.clearItems();
        }
    }

    @Override
    public boolean receiveInteraction(Character agent, Item itemUsedToInteract) {
        System.out.println("Tile receiving interaction...");
        if(agent.getGameTag().equals("player")) {
            System.out.println("Interacting agent is the Player");
            if(this.isDiggable()) {
                System.out.println("Player is digging the wall... " + this.getType());
                this.setType(Tiles.FLOOR.toString());
                this.setGlyph(new Glyph(new Color(255, 255, 255) , '.'));
                System.out.println("New Tile Type: " + this.getType());
                return true;
            }
        }
        System.out.println("No interaction was possible");
        return false;
    }

    public List<Tile> getNeighbours(World world) {
        return world.getNeighbouringTiles(this.xPos, this.yPos);
    }

    public boolean isTraversable() {
        if(this.type.equals(Tiles.FLOOR.toString())) {
            return true;
        }
        return false;
    }

    public boolean freeToWalk() {
        if(isTraversable() && !hasCharacter()) {
            return true;
        }
        return false;
    }

    @Override
    public Position getPosition() {
        return new Position(this.xPos, this.yPos);
    }
    @Override
    public void setPosition(Position pos) {
        this.xPos = pos.getX();
        this.yPos = pos.getY();
    }

    public boolean isToBeRemoved() {
        return this.toBeRemoved;
    }
}
