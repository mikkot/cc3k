package engine.world;

public interface MapGenerator {
    MapGenerator generateTiles();
    Tile[][] build();
    MapGenerator smooth(int iterations);
}
