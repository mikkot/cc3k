package engine.world;

import java.awt.*;

public class Glyph {
    private Color color;
    private char glyph;

    public Glyph() {
        this.color = new Color(255, 255, 255);
        this.glyph = '.';
    }

    public Glyph(Color color, char glyph) {
        this.color = color;
        this.glyph = glyph;
    }

    public void setGlyph(char glyph) { this.glyph = glyph; }
    public char getGlyph() { return this.glyph; }

    public void setColor(Color color) { this.color = color; }
    public void setColor(int r, int g, int b) { this.color = new Color(r, g, b); }
    public Color getColor() { return this.color; }
}
