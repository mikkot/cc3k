package engine.world;

import engine.world.Tiles;

import java.awt.*;

public class RandomMapGeneratorOne implements MapGenerator {
    private int width;
    private int height;
    private Tile[][] gameMap;
    private final Color WALL_COLOR = new Color(89, 73, 36);
    private final Color FLOOR_COLOR = new Color(112, 173, 2);

    public RandomMapGeneratorOne(int width, int height) {
        this.width = width;
        this.height = height;
        this.gameMap = new Tile[width][height];
    }

    @Override
    public MapGenerator generateTiles() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if(y == 0 || y == gameMap[0].length-1 || x == 0 || x== gameMap.length-1)  {
                    gameMap[x][y] = new Tile(x, y, Tiles.WALL.toString(), new Glyph(this.WALL_COLOR, '#'), true);
                } else {
                    //TODO: Make the floor/wall probability configurable
                    gameMap[x][y] = Math.random() < 0.55 ? new Tile(x, y, Tiles.FLOOR.toString(), new Glyph(this.FLOOR_COLOR, '.'), false) : new Tile(x, y, Tiles.WALL.toString(), new Glyph(this.WALL_COLOR, '#'), false);
                }
            }
        }
        return this;
    }

    @Override
    public MapGenerator smooth(int iterations) {
        Tile[][] tiles2 = new Tile[width][height];
        for (int iteration = 0; iteration < iterations; iteration++) {

            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    int floors = 0;
                    int walls = 0;

                    for (int ox = -1; ox < 2; ox++) {
                        for (int oy = -1; oy < 2; oy++) {
                            if (x + ox < 0 || x + ox >= width || y + oy < 0
                                    || y + oy >= height)
                                continue;

                            if (gameMap[x + ox][y + oy].getType().equals(Tiles.FLOOR.toString()))
                                floors++;
                            else
                                walls++;
                        }
                    }
                    tiles2[x][y] = floors >= walls ?
                            new Tile(x, y, Tiles.FLOOR.toString(), new Glyph(this.FLOOR_COLOR, '.'), false) :
                            new Tile(x, y, Tiles.WALL.toString(), new Glyph(this.WALL_COLOR, '#'), true);

                    if(tiles2[x][y].getType().equals(Tiles.WALL.toString()) && (y == 0 || y == height-1)) {
                        tiles2[x][y] = new Tile(x, y, Tiles.WALL.toString(), new Glyph(this.WALL_COLOR, '#'), true);
                    }
                }
            }
            gameMap = tiles2;
        }
        return this;
    }

    public Tile[][] build() {
        return this.gameMap;
    }
}
