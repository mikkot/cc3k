package engine.world;

import entity.character.Character;

public interface IEnterBehavior {
    public void onEnter(Tile tile, Character character);
    public boolean isActive();
}
