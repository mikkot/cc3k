package engine.world;

import engine.Position;
import entity.character.Character;

public class EnterTeleport implements IEnterBehavior {
    protected Position teleportDestination;
    protected boolean isActive = true;

    public EnterTeleport() { this.teleportDestination = new Position(1, 1); }
    public EnterTeleport(Position teleportDestination) { this.teleportDestination = teleportDestination; }
    public EnterTeleport(int x, int y) { this(new Position(x, y)); }

    public void setTeleportDestination(Position position) { this.teleportDestination = position; }
    public void setGoal(int x, int y) { this.teleportDestination = new Position(x, y); }
    public Position getTeleportDestination() { return this.teleportDestination; }
    public boolean getActive() { return this.isActive; }
    public void setActive(boolean active) { this.isActive = active; }

    public void onEnter(Tile tile, Character character) {
        //Trigger Change level somehow. How to get reference to World?
        System.out.println("TELEPORT TO " + this.teleportDestination.getX() + "," + this.teleportDestination.getY());
        character.setPosition(getTeleportDestination());
    }

    public boolean isActive() { return getActive(); }
}
