package engine;

import asciiPanel.AsciiPanel;
import engine.common.ILogger;
import engine.common.SimpleLogger;
import engine.screen.Screen;
import engine.screen.StartScreen;

import javax.swing.*;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //AsciiPanel terminal = new AsciiPanel(80, 27);
        AsciiPanel terminal = new AsciiPanel(320, 108);
        Screen screen = new StartScreen(320, 108);

        Game game = new Game(terminal, screen);
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setVisible(true);
    }
}
