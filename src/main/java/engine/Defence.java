package engine;

public enum Defence {
    BLUNT,
    PIERCING,
    SLASH,
    POISON,
    MAGIC,
    POTION,
}
