package engine;

import engine.common.messagequeue.Message;

import java.util.Observable;
import java.util.UUID;

public class GameObject extends Observable {
    public String tag;

    public GameObject() {
        this.tag = UUID.randomUUID().toString();
    }

    public String getGameTag() {
        return this.tag;
    }

    public void receiveMessage(Message message) {}

}
