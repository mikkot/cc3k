package engine;

import asciiPanel.AsciiPanel;
import entity.character.Character;
import engine.screen.Screen;
import entity.character.Player;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Game extends JFrame implements KeyListener {
    private AsciiPanel terminal;
    private Screen screen;

    public Game(AsciiPanel terminal, Screen screen) {
        super();
        this.terminal = terminal;
        this.screen = screen;
        add(terminal);
        pack();
        addKeyListener(this);
        repaint();
    }

    public void repaint() {
        terminal.clear();
        screen.displayOutput(terminal);
        super.repaint();
    }

    public void keyPressed(KeyEvent key) {
        screen = screen.respondToUserInput(key);
        repaint();
    }
    public void keyReleased(KeyEvent key) {}
    public void keyTyped(KeyEvent key) {}


}
