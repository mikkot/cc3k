package engine;

public class Position {
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setPosition(Position newPosition) {
        setX(newPosition.getX());
        setY(newPosition.getY());
    }

    public void setPosition(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

    //TODO: make it clear that the parameters are increments to existing positions, not new positions.
    public void move(int x, int y) {
        this.x += x;
        this.y += y;
        return;
    }

    public void move(Position pos) {
        this.x += pos.getX();
        this.y += pos.getY();
    }

    public boolean isBetween(Position topLeft, Position bottomRight) {
        if(this.getX() > topLeft.getX() && this.getX() < bottomRight.getX() && this.getY() > topLeft.getY() && this.getY() < bottomRight.getY()) {
            return true;
        }
        return false;
    }

    public boolean isNextTo(Position position) {
        //If Y and X axis are within 1 step from each other respectively, the entities must be adjacent
        if(Math.abs(this.getY() - position.getY()) <= 1 && Math.abs(this.getX() - position.getX()) <= 1) {
            return true;
        }
        return false;
    }
}
