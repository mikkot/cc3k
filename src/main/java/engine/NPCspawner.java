package engine;

import engine.world.Glyph;
import entity.character.*;

import java.awt.*;
import java.lang.Character;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class NPCspawner {
    private HashMap<String, NPC> npcClasses;
    private HashMap<String, Character> glyphs;
    private RandomMove randomMove;
    private Random random;

    public NPCspawner(RandomMove randomMove, Random random) {
        this.randomMove = randomMove;
        this.random = random;
        this.npcClasses = new HashMap<>();
        this.glyphs = new HashMap<>();
        this.populateGlyphMap();
    }

    private void populateGlyphMap() {
        this.glyphs.put(NPCs.WEREWOLF.toString(), 'W');
        this.glyphs.put(NPCs.VAMPIRE.toString(), 'V');
        this.glyphs.put(NPCs.TROLL.toString(), 'T');
        this.glyphs.put(NPCs.GOBLIN.toString(), 'G');
        this.glyphs.put(NPCs.ORC.toString(), 'O');
        this.glyphs.put(NPCs.RAT.toString(), 'R');
        this.glyphs.put(NPCs.UNKNOWN.toString(), 'U');
    }

    //Creates NPC subclasses, the Positions etc. need to be set after instantiating
    public List<NPC> spawnNPCsOfTypeAndAmount(NPC prototype, int amount) {
        List<NPC> createdNPCs = new ArrayList<>();

        //TODO: Return empty list, if the type is not available. Maybe should throw error or something?
        if(prototype == null) {
            return createdNPCs;
        }

        for(int i = 0; i<amount; i++) {
            NPC npc = prototype.clone();
            createdNPCs.add(npc);
        }
        return createdNPCs;
    }

    public NPC getRandomNPCType() {
        //Object[] keys = this.npcClasses.keySet().toArray();
        //int maxValue = keys.length;

        //Object obj = keys[random.nextInt(maxValue)];
        //return this.npcClasses.get(obj);
        return null;
    }

    public List<NPC> createRandomTypeNPCsOfAmount(int amount) {
        List<NPC> npcs = new ArrayList<>();
        NPCs[] npcRaces = NPCs.values();
        for(int i = 0; i<amount; i++) {
            NPC prototype = new NPC();
            prototype.setMoveBehavior(new MoveRandom());

            // Pick out a random NPC race for each NPC
            String npcRace = npcRaces[random.nextInt(this.glyphs.size())].toString();
            prototype.setGlyph(new Glyph(new Color(255, 255, 255), this.glyphs.get(npcRace)));
            prototype.setRace(npcRace);
            npcs.add(prototype);
        }
        return npcs;
        //return spawnNPCsOfTypeAndAmount(prototype, amount);
    }

    public List<NPC> createNPCsOfRandomTypeAndRandomAmount(int maxAmount) {
        int amount = random.nextInt(maxAmount);
        return createRandomTypeNPCsOfAmount(amount);
    }
}
