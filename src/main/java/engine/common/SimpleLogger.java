package engine.common;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import entity.character.Character;

public class SimpleLogger implements ILogger, PropertyChangeListener {
    private List<String> logs = new ArrayList<>();

    public SimpleLogger() {}


    @Override
    public List<String> getFullLog() {
        return this.logs;
    }

    @Override
    public List<String> getLatestLogs(int amount) {
        System.out.println("Original log length: " + this.logs.size());
        if(this.logs.size() == 0) return null;
        if(this.logs.size() < amount) return this.logs;
        return this.logs.subList(this.logs.size() - amount, this.logs.size()); // Might be dangerous, as changes this subList apparently change the original as well?
    }

    @Override
    public void clearLog() {
        this.logs.clear();
    }

    @Override
    public void addLogEvent(String eventDescription) {
        this.logs.add(eventDescription);
    }

    @Override
    public void addMultipleLogEvents(List<String> eventDescriptions) {
        for(String eventDescription : eventDescriptions) {
            addLogEvent(eventDescription);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(this.logs.size() > 100) this.clearLog(); // Simple precaution so we don't fill up memory with the logs
        String changedPropertyName = evt.getPropertyName();
        // Right now we can assume that only Characters will have these changelisteners attached
        Character targetCharacter = (Character) evt.getSource();
        switch (changedPropertyName) {
            case "health":
                this.addLogEvent(
                        targetCharacter.getRace() +
                                ((Integer) evt.getOldValue() <  (Integer) evt.getNewValue() ? " gained" : " lost ") +
                                Math.abs((Integer) evt.getOldValue() - (Integer) evt.getNewValue()) + " health"
                );
                break;
        }
    }
}
