package engine.common.messagequeue;

import java.util.List;

public interface IMessageBroker {
    void sendMessage(String message, MessageQueues queue);
    void sendMessage(String message, MessageQueues queue, String receiverId);
    void sendMultipleMessages(List<String> messages, MessageQueues queue);
}
