package engine.common.messagequeue;

import java.util.HashMap;

public class MessageQueueManager {
    private HashMap<String, MessageQueue> messageQueues;

    public MessageQueueManager() {
        this.messageQueues = new HashMap<>();
        // We create all the predefined message queues when we instantiate a MessageQueueManager
        this.messageQueues.put(MessageQueues.GAME_OBJECT.toString(), new MessageQueue());
        this.messageQueues.put(MessageQueues.NPC.toString(), new MessageQueue());
        this.messageQueues.put(MessageQueues.PLAYER.toString(), new MessageQueue());
        this.messageQueues.put(MessageQueues.ITEM.toString(), new MessageQueue());
        this.messageQueues.put(MessageQueues.USER_NOTIFICATION.toString(), new MessageQueue());
    }

    public MessageQueue getMessageQueue(MessageQueues queue) {
        return this.messageQueues.get(queue.toString());
    }
    public MessageQueue getMessageQueue(String queue) {
        return this.messageQueues.get(queue);
    }
}
