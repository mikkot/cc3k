package engine.common.messagequeue;

import java.util.List;

public class DefaultMessageBroker implements IMessageBroker {
    private MessageQueueManager queueManager;

    public DefaultMessageBroker() { this.queueManager = new MessageQueueManager(); }
    public DefaultMessageBroker(MessageQueueManager queueManager) { this.queueManager = queueManager; }

    @Override
    public void sendMessage(String message, MessageQueues queue) {
        this.queueManager.getMessageQueue(queue).addMessage(message);
    }

    @Override
    public void sendMessage(String message, MessageQueues queue, String receiverId) {
        this.queueManager.getMessageQueue(queue).addMessage(message, receiverId);
    }

    @Override
    public void sendMultipleMessages(List<String> messages, MessageQueues queue) {
        for(String message : messages) {
            this.queueManager.getMessageQueue(queue).addMessage(message);
        }
    }
}
