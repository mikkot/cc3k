package engine.common.messagequeue;

public enum MessageQueues {
    GAME_OBJECT,
    NPC,
    PLAYER,
    ITEM,
    USER_NOTIFICATION
}
