package engine.common.messagequeue;

import engine.GameObject;

import java.util.*;

public class MessageQueue {
    private HashMap<String, GameObject> subscribers;
    private Queue<Message> messages;

    public MessageQueue() {
        this.subscribers = new HashMap<>(); this.messages = new ArrayDeque<>();
    }
    public MessageQueue(HashMap<String, GameObject> subscribers) {
        this.subscribers = subscribers;
        this.messages = new ArrayDeque<>();
    }

    public void addSubscriber(GameObject gameObject) {
        this.subscribers.put(gameObject.getGameTag(), gameObject);
    }
    public void removeSubscriber(String tag) {
        this.subscribers.remove(tag);
    }

    public List<GameObject> getSubscribers() {
        return new ArrayList<>(this.subscribers.values());
    }
    public void setSubscribers(HashMap<String, GameObject> newSubscribers) {
        this.subscribers = newSubscribers;
    }
    public int getQueueLength() { return this.messages.size(); }

    public boolean isObjectSubscribed(String tag) {
        return this.subscribers.containsKey(tag);
    }

    public void addMessage(Message message) {
        this.messages.add(message);
    }
    public void addMessage(String message) {
        this.messages.add(new Message(message));
    }
    public void addMessage(String message, String receiverId) {
        Message msg = new Message(message, receiverId);
        this.messages.add(msg);
    }
    public Message consumeNextMessage() {
        return this.messages.poll();
    }
    public Message peekNextMessage() {
        return this.messages.peek();
    }

    private void notifySubscriber(Message message) {
        this.subscribers.get(message.getTargetObject()).receiveMessage(message);
    }
}
