package engine.common.messagequeue;

public class Message {
    String message;
    boolean delivered = false;
    String targetObject = "";

    public Message() {

    }
    public Message(String message) {
        this.message = message;
    }
    public Message(String message, String targetObject) {
        this.message = message;
        this.targetObject = targetObject;
    }

    // Getters/Setters
    public boolean isDelivered() {
        return this.delivered;
    }

    public String getMessage() {
        return this.message;
    }

    public String getTargetObject() {
        return this.targetObject;
    }

    public void setTargetObject(String targetObject) {
        this.targetObject = targetObject;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
