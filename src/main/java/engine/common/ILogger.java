package engine.common;

import java.util.List;

public interface ILogger {

    List<String> getFullLog();
    List<String> getLatestLogs(int amount);
    void clearLog();
    void addLogEvent(String eventDescription);
    void addMultipleLogEvents(List<String> eventDescriptions);
}
