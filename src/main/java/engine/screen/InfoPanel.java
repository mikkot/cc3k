package engine.screen;

import asciiPanel.AsciiPanel;
import engine.common.ILogger;
import engine.common.SimpleLogger;
import entity.character.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class InfoPanel {
    private final int INVENTORY_OFFSET_Y = 5;
    private final int INVENTORY_OFFSET_X = 20;
    private final int INVENTORY_ITEM_OFFSET_X = 25;

    // The offsets could be calculated from the ActionLog size and PlayerInfo size instead.
    private final int ACTION_LOG_OFFSET_X = 15;
    private final int ACTION_LOG_OFFSET_Y = 23;

    private final int PLAYER_INFO_WIDTH = 15;
    private final int PLAYER_INFO_HEIGHT = 4;
    private final int ACTION_LOG_HEIGHT = 4;

    private final int PLAYER_INFO_OFFSET_X = 0;

    private ILogger logger;

    public InfoPanel(ILogger logger) {
        this.logger = logger;
    }

    public void print(AsciiPanel terminal, Player player) {
        this.printPlayerStats(terminal, player);
        this.printActionLog(terminal);
    }

    private void printPlayerStats(AsciiPanel terminal, Player player) {
        terminal.write("|| Health: " + player.getHealth(), this.PLAYER_INFO_OFFSET_X, terminal.getHeightInCharacters() - this.PLAYER_INFO_HEIGHT, new Color(255, 0, 0))
                .write("|| Defence: " + player.getDefence(), this.PLAYER_INFO_OFFSET_X, terminal.getHeightInCharacters() - this.PLAYER_INFO_HEIGHT + 1, new Color(0, 255, 0))
                .write("|| Attack: " + player.getAttack(), this.PLAYER_INFO_OFFSET_X, terminal.getHeightInCharacters() - this.PLAYER_INFO_HEIGHT + 2, new Color(0, 0, 255))
                .write("||", this.PLAYER_INFO_OFFSET_X, terminal.getHeightInCharacters() - this.PLAYER_INFO_HEIGHT + 3);
    }

    private void printEnemyStats(AsciiPanel terminal, Player player) {
        // Here get the enemy and print its stats.
    }

    private void printActionLog(AsciiPanel terminal) {
        System.out.println("printActionLog");
        int terminalHeight = terminal.getHeightInCharacters();

        List<String> latestLogs = logger.getLatestLogs(4);
        if(latestLogs == null || latestLogs.size() == 0) return;
        //latestLogs = Arrays.asList("Item 1", "Item 2", "Item 3", "Item 4");
        System.out.println(latestLogs.size());
        for(int i = 0; i<latestLogs.size(); i++) {
            terminal.write("|| " + latestLogs.get(i), this.ACTION_LOG_OFFSET_X, terminalHeight - this.ACTION_LOG_HEIGHT + i);
        }
    }
}
