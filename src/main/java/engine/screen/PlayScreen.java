package engine.screen;

import asciiPanel.AsciiPanel;
import engine.Position;
import engine.common.ILogger;
import engine.world.Tile;
import engine.world.World;
import entity.character.Player;
import entity.character.Character;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class PlayScreen implements Screen {
    private ILogger logger;

    private final World world;
    private final InfoPanel infoPanel;
    private final int screenWidth;
    private final int screenHeight;
    private int centerX;
    private int centerY;
    private int cursorLimitLeftX;
    private int cursorLimitRightX;
    private int cursorLimitTopY;
    private int cursorLimitBottomY;
    //These are to stop printing the World Map to leave room on the Screen for other things, like InfoPanel etc.
    private int BOTTOM_MARGIN = 4;
    private int RIGHT_MARGIN = 0;

    public PlayScreen(World world, ILogger logger, int screenWidth, int screenHeight) {
        this.logger = logger;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.world = world;
        this.infoPanel = new InfoPanel(this.logger);
        // These limit how much the Camera screen can 'move' on the Map. The Camera is tracked by its middle point.
        this.cursorLimitLeftX = (screenWidth - RIGHT_MARGIN) / 2;
        this.cursorLimitRightX = Math.max(screenWidth / 2, screenWidth / 2 + (world.getMapWidth() - screenWidth)); // world.getMapWidth() - (screenWidth - RIGHT_MARGIN) / 2;
        this.cursorLimitTopY = (screenHeight - BOTTOM_MARGIN) / 2;
        this.cursorLimitBottomY = Math.max(screenHeight / 2, screenHeight / 2 + (world.getMapHeight() - screenHeight)); // world.getMapHeight() - (screenHeight - BOTTOM_MARGIN) / 2;
        //Max picks the Player position for the Camera if the Player is not in the top-left corner/close to it, in case
        //we are returning to the PlayScreen from some other screen, and we don't want to reset the Camera to the
        //top-left corner.
        this.centerX = Math.max(cursorLimitLeftX, this.world.getPlayer().getPosition().getX());
        this.centerY = Math.max(cursorLimitTopY, this.world.getPlayer().getPosition().getY());
    }

    /**
     *
     * @return int The distance of the Camera's left edge from the Map's left edge
     */
    public int getScrollX() {
        return Math.max(0, Math.min(centerX - cursorLimitLeftX, world.getMapWidth() - (screenWidth - RIGHT_MARGIN)));
    }

    /**
     *
     * @return int The distance of the Camera's top edge from the Map's top edge
     */
    public int getScrollY() {
        return Math.max(0, Math.min(centerY - cursorLimitTopY, world.getMapHeight() - (screenHeight - BOTTOM_MARGIN)));
    }

    //Max - picks the limit if the player scrolls below it, otherwise picks the current position
    //Min - picks the current position unless it is beyond the limit to Right or Bottom
    //This is so the 'cursor' which guides the camera, never moves if the Screen has no space to move (hits Map wall)
    public void scrollBy(int _x, int _y) {
        centerX = Math.max(this.cursorLimitLeftX, Math.min(centerX + _x, this.cursorLimitRightX));
        centerY = Math.max(this.cursorLimitTopY, Math.min(centerY + _y, this.cursorLimitBottomY));
    }
    public void scrollTo(int x, int y) {
        centerX = Math.max(this.cursorLimitLeftX, Math.min(x, this.cursorLimitRightX));
        centerY = Math.max(this.cursorLimitTopY, Math.min(y, this.cursorLimitBottomY));
    }

    public void displayOutput(AsciiPanel terminal) {
        int startX = getScrollX();
        int startY = getScrollY();
        this.printMap(terminal, startX, startY, RIGHT_MARGIN, BOTTOM_MARGIN);
        infoPanel.print(terminal, (Player) this.world.getPlayer());
    }

    public void printMap(AsciiPanel terminal, int beginningX, int beginningY, int endX, int endY) {
        // We only want to print until the end of our screen (obviously, we can not tell the screen to print something
        // that is beyond its borders) OR until the end of the map edges, in case the whole Map fits inside the screen.
        int limitY = Math.min(terminal.getHeightInCharacters() - endY, this.world.getMapHeight());
        int limitX = Math.min(terminal.getWidthInCharacters() - endX, this.world.getMapWidth());
        // y must be smaller than the world's dimensions, but also limited by the Camera height, so we only print as much
        // of the World that we want the Camera to show
        for(int y = 0; y<limitY; y++) {
            for(int x = 0; x<limitX; x++) {
                int _x = Math.min(x + beginningX, this.world.getGameMap().length-1);
                int _y = Math.min(y + beginningY, this.world.getGameMap()[0].length-1);
                terminal.write(this.world.getGameMap()[_x][_y].getGlyph().getGlyph(), x, y, this.world.getGameMap()[_x][_y].getGlyph().getColor());
            }
        }
    }

    /**
     *
     * @param target Position of the GameObject on the World Map
     * @return boolean
     */
    public boolean isInCameraView(Position target) {
        int targetX = target.getX();
        int targetY = target.getY();
        int cameraLeft = getScrollX();
        int cameraTop = getScrollY();

        return targetX >= cameraLeft && targetX < cameraLeft + this.screenWidth - RIGHT_MARGIN && targetY >= cameraTop && targetY < cameraTop + this.screenHeight - BOTTOM_MARGIN;
    }

    public Screen respondToUserInput(KeyEvent key) {
        Player player = (Player) this.world.getPlayer();
        if(key.getKeyCode() == KeyEvent.VK_ESCAPE) {
            return new StartScreen();
        }
        //TODO: move this to another class
        if(key.getKeyCode() > 47 && key.getKeyCode() < 58) {
            //Attack/Interact with corresponding NPCs
            List<Tile> neighbouringTiles = player.getNeighbours(this.world);
            List<Character> charactersNextTo = new ArrayList<>();
            for(Tile t : neighbouringTiles) {
                if(t.hasCharacter()) {
                    charactersNextTo.add(t.getCharacter());
                }
            }
            if(!charactersNextTo.isEmpty()) { player.attack(charactersNextTo.get(0)); }
            return this;
        }

        switch (key.getKeyCode()) {
            case KeyEvent.VK_W:
                player.move(this.world.getGameMap(), 0, -1);
                scrollTo(player.getPosition().getX(), player.getPosition().getY());
                break;
            case KeyEvent.VK_D:
                player.move(this.world.getGameMap(), 1, 0);
                scrollTo(player.getPosition().getX(), player.getPosition().getY());
                break;
            case KeyEvent.VK_A:
                player.move(this.world.getGameMap(), -1, 0);
                scrollTo(player.getPosition().getX(), player.getPosition().getY());
                break;
            case KeyEvent.VK_S:
                player.move(this.world.getGameMap(), 0, 1);
                scrollTo(player.getPosition().getX(), player.getPosition().getY());
                break;
            case KeyEvent.VK_SPACE:
                // Maybe worth thinking about if 'interact' and 'collect' could be the same thing?
                player.collect(this.world.getGameMap());
                break;
            case KeyEvent.VK_UP:
                scrollBy( 0,-1);
                break;
            case KeyEvent.VK_LEFT:
                scrollBy(-1, 0);
                break;
            case KeyEvent.VK_DOWN:
                scrollBy( 0, 1);
                break;
            case KeyEvent.VK_RIGHT:
                scrollBy( 1, 0);
                break;
            case KeyEvent.VK_C:
                return new PlayerScreen(this.world, this.logger);
            case KeyEvent.VK_E:
                player.interact(this.world);
                break;
            default: break;
        }

        world.updateGameEntities();
        return this;
    }
}
