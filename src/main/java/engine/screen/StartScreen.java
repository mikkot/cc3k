package engine.screen;

import asciiPanel.AsciiPanel;
import engine.NPCspawner;
import engine.RandomMove;
import engine.common.SimpleLogger;
import engine.world.RandomMapGeneratorOne;
import engine.world.World;

import java.awt.event.KeyEvent;
import java.util.Random;

public class StartScreen implements Screen {

    private int mapWidth;
    private int mapHeight;

    private int screenWidth;
    private int screenHeight;

    public StartScreen() { super(); }
    public StartScreen(int screenWidth, int screenHeight) {
        super();
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }
    public void displayOutput(AsciiPanel terminal) {
        terminal.writeCenter("Pick World size and press Enter:", 22)
                .writeCenter("1. 90x25", 23)
                .writeCenter("2. 180x50", 24)
                .writeCenter("3. 360x100", 25);

    }

    public Screen respondToUserInput(KeyEvent key) {
        switch (key.getKeyCode()) {
            case KeyEvent.VK_1:
                this.mapHeight = 25;
                this.mapWidth = 90;
                break;
            case KeyEvent.VK_2:
                this.mapHeight = 50;
                this.mapWidth = 180;
                break;
            case KeyEvent.VK_3:
                this.mapHeight = 100;
                this.mapWidth = 360;
                break;
        }
        if(key.getKeyCode() == KeyEvent.VK_ENTER) {
            SimpleLogger logger = new SimpleLogger();
            World world = new World(
                    this.mapWidth,
                    this.mapHeight,
                    new Random(),
                    new NPCspawner(new RandomMove(new Random()), new Random()),
                    new RandomMapGeneratorOne(this.mapWidth, this.mapHeight),
                    logger
            );
            world.generateMap();
            world.createRandomLevelItems(10);
            world.createRandomLevelNPCs(10);
            return new PlayScreen(world, logger, this.screenWidth, this.screenHeight);
        }
        return this;
    }
}
