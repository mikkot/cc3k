package engine.screen;

import asciiPanel.AsciiPanel;
import engine.common.ILogger;
import engine.world.World;
import entity.character.AggregatedInventoryItem;
import entity.character.Character;
import entity.character.Player;
import entity.item.Item;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlayerScreen implements Screen {
    private World world;
    private ILogger logger;
    private int screenWidth;
    private int screenHeight;
    private int verticalDivider;
    private Map<String, Integer> aggregatedInventoryItems;
    private List<AggregatedInventoryItem> inventoryItems;

    public PlayerScreen(World world, ILogger logger) {
        this.world = world;
        this.logger = logger;
        this.inventoryItems = new ArrayList<>();
    }

    public void displayOutput(AsciiPanel terminal) {
        this.screenWidth = terminal.getWidthInCharacters();
        this.screenHeight = terminal.getHeightInCharacters();
        this.verticalDivider = screenWidth % 2 == 0 ? screenWidth / 2 : (screenWidth + 1) / 2;
        //Print Player stats and skills on left side, and Player Inventory on right side

        printScreenEdges(terminal);
        printPlayerStats(terminal);
        printPlayerInventory(terminal);

    }

    public void printPlayerStats(AsciiPanel terminal) {
        int offset_x = 3;
        int offset_y = 3;
        int limit_x = verticalDivider - 2;

        Character player = world.getPlayer();

        terminal.write("Player:", offset_x, offset_y - 1);

        terminal.write("Health: " + player.getHealth(), offset_x, offset_y);
        terminal.write("Attack: " + player.getAttack(), offset_x, offset_y + 1);
        terminal.write("Defence: " + player.getDefence(), offset_x, offset_y + 2);
        terminal.write("Level: " + player.getLevel(), offset_x, offset_y + 3);
        terminal.write("XP: " + player.getXp() + "/" + player.getXpLimit(), offset_x, offset_y + 4);

    }
    public void printPlayerInventory(AsciiPanel terminal) {
        int offset_x = verticalDivider + 3;
        int offset_y = 3;
        int limit_x = screenWidth - 2;

        this.aggregatedInventoryItems = world.getPlayer().getInventory().getAggregatedAndSortedItems();
        terminal.write("Inventory:", offset_x, offset_y - 1);
        //int currentLine = this.INVENTORY_OFFSET_Y + 1;
        int currentLine = offset_y;
        for(String s : this.aggregatedInventoryItems.keySet()) {
            terminal.write( (currentLine - offset_y) + ". " + s + " x" + this.aggregatedInventoryItems.get(s), offset_x, currentLine);
            this.inventoryItems.add(new AggregatedInventoryItem(s, this.aggregatedInventoryItems.get(s)));
            currentLine++;
        }
    }

    public void printScreenEdges(AsciiPanel terminal) {
        for(int i = 0; i<screenWidth; i++) {
            terminal.write('=', i, 0);
            terminal.write('=', i, screenHeight-1);
        }
        //Left and right and divider
        for(int i = 0; i<screenHeight; i++) {
            terminal.write('|', 0, i);
            terminal.write('|', verticalDivider, i);
            terminal.write('|', screenWidth - 1, i);
        }
    }

    public void useInventoryItem(int itemIndex) {
        int offsetIndex = 48 - itemIndex;
        Player player = (Player) this.world.getPlayer();
        String itemType = this.inventoryItems.get(offsetIndex).getType();
        Item selectedItem = player.getInventory().getItem(itemType);
        player.useItem(selectedItem);
    }

    public void nextInventoryPage() {
        System.out.println("Next Inventory page...");
        //Do we page the Inventory here, or in InventoryBehavior class, or some new component?
    }
    public void previousInventoryPage() {
        System.out.println("Previous Inventory page...");
        //Do we page the Inventory here, or in InventoryBehavior class, or some new component?
    }

    @Override
    public Screen respondToUserInput(KeyEvent key) {
        if(key.getKeyCode() > 47 && key.getKeyCode() < 58) {
            useInventoryItem(key.getKeyCode());
            return this;
        }
        switch (key.getKeyCode()) {
            case KeyEvent.VK_C:
                return new PlayScreen(this.world, logger, this.screenWidth, this.screenHeight);
            case KeyEvent.VK_Q:
                previousInventoryPage();
                break;
            case KeyEvent.VK_E:
                nextInventoryPage();
                break;
            default: break;
        }
        return this;
    }
}
